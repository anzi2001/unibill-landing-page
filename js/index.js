new fullpage('#fullpage', {
	//options here
	licenseKey:'OPEN-SOURCE-GPLV3-LICENSE',
	autoScrolling:true,
	scrollHorizontally: true,
	scrollOverflow:true,
	paddingTop:'60px'
});
var navbar = document.getElementById("navbar");
for(var i = 0;i<navbar.children.length;i++){
	navbar.children[i].addEventListener("click",(event)=>{
		if(event.target.tagName == "P"){
			fullpage_api.moveTo(Number(event.target.getAttribute("label")));
		}
		else if(event.target.tagName == "A"){
			fullpage_api.moveTo(Number(event.target.children[0].getAttribute("label")));			
		}
		else{
			fullpage_api.moveTo(Number(event.target.children[0].children[0].getAttribute("label")));
		}
	})
}
//methods